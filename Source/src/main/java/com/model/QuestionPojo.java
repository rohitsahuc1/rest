package com.model;

public class QuestionPojo {
	private String question_id;
	public String getQuestion_id() {
		return question_id;
	}
	public void setQuestion_id(String question_id) {
		this.question_id = question_id;
	}
	private String question;
	
	
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	@Override
	public String toString() {
		return getQuestion();
	}
	public QuestionPojo(String question_id, String question) {
		super();
		this.question_id = question_id;
		this.question = question;
	}
	

}
