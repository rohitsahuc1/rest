package com.model;

import java.sql.Timestamp;
import java.util.List;

import com.jayway.jsonpath.Predicate;

public class Order {
	private String ID;
	private String CUSTOMER;
	private String TOTAL_PRICE;
	private Timestamp TIMESTAMP;
	private String ADDRESS;
	private String CARD_NO;
	private String CVV;
	private List<ProductPojo> ITEMS;

	public Order(String iD, String cUSTOMER, String tOTAL_PRICE, Timestamp tIMESTAMP, String aDDRESS, String cARD_NO,
			String cVV, List<ProductPojo> iTEMS) {
		super();
		ID = iD;
		CUSTOMER = cUSTOMER;
		TOTAL_PRICE = tOTAL_PRICE;
		TIMESTAMP = tIMESTAMP;
		ADDRESS = aDDRESS;
		CARD_NO = cARD_NO;
		CVV = cVV;
		ITEMS = iTEMS;
	}

	public Order() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getCUSTOMER() {
		return CUSTOMER;
	}

	public void setCUSTOMER(String cUSTOMER) {
		CUSTOMER = cUSTOMER;
	}

	public String getTOTAL_PRICE() {
		return TOTAL_PRICE;
	}

	public void setTOTAL_PRICE(String tOTAL_PRICE) {
		TOTAL_PRICE = tOTAL_PRICE;
	}

	public Timestamp getTIMESTAMP() {
		return TIMESTAMP;
	}

	public void setTIMESTAMP(Timestamp tIMESTAMP) {
		TIMESTAMP = tIMESTAMP;
	}

	public String getADDRESS() {
		return ADDRESS;
	}

	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}

	public String getCARD_NO() {
		return CARD_NO;
	}

	public void setCARD_NO(String cARD_NO) {
		CARD_NO = cARD_NO;
	}

	public String getCVV() {
		return CVV;
	}

	public void setCVV(String cVV) {
		CVV = cVV;
	}

	public List<ProductPojo> getITEMS() {
		return ITEMS;
	}

	public void setITEMS(List<ProductPojo> iTEMS) {
		ITEMS = iTEMS;
	}
}
