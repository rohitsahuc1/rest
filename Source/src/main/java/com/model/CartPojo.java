package com.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

public class CartPojo {
	private List<ProductPojo> items;
	private int total;
	public int getTotal() {
		return total;
	}

	public CartPojo(List<ProductPojo> items, int total) {
		super();
		this.items = items;
		this.total = total;
	}

	public void calculateTotal() {
		total = 0;
		for (int i = 0; i < items.size(); i++) {
			total += Integer.parseInt(items.get(i).getPRICE());
		}
	}

	@Override
	public String toString() {
		return "CartPojo [items=" + items + ", total=" + total + "]";
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public void addToCart(ProductPojo p) {
		items.add(p);
		calculateTotal();
	}

	public void removeFromCart(int p) {
		for (int i = 0; i < items.size(); i++) {
			if (items.get(i).getID() == p) {
				items.remove(i);
				break;
			}
		}
		calculateTotal();
	}

	public List<ProductPojo> getItems() {
		System.out.println(items);
		return items;
	}

	public void setItems(List<ProductPojo> items) {
		this.items = items;
	}

}
