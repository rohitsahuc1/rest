package com.model;

import java.sql.Date;

public class FilterPojo {
private Date from;
private Date end;
public Date getFrom() {
	return from;
}
public void setFrom(Date from) {
	this.from = from;
}
public Date getEnd() {
	return end;
}
public void setEnd(Date end) {
	this.end = end;
}

}
