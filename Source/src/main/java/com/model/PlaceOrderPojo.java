package com.model;

import java.util.List;

import javax.validation.constraints.Size;

public class PlaceOrderPojo {
private String ADDRESS;
@Size(min=16, max=16)
private String CARD_NUMBER;
@Size(min=3, max=3)
private String CVV;
private String TOTAL_PRICE;
private String TIMESTAMP;
private String CUSTOMER;

private List<ProductPojo> items;
public List<ProductPojo> getItems() {
	return items;
}
public void setItems(List<ProductPojo> items) {
	this.items = items;
}
public String getADDRESS() {
	return ADDRESS;
}
public void setADDRESS(String aDDRESS) {
	ADDRESS = aDDRESS;
}
public String getCARD_NUMBER() {
	return CARD_NUMBER;
}
public void setCARD_NUMBER(String cARD_NUMBER) {
	CARD_NUMBER = cARD_NUMBER;
}
public String getCVV() {
	return CVV;
}

@Override
public String toString() {
	return "PlaceOrderPojo [ADDRESS=" + ADDRESS + ", CARD_NUMBER=" + CARD_NUMBER + ", CVV=" + CVV + ", items=" + items
			+ ", TOTAL_PRICE=" + TOTAL_PRICE + ", TIMESTAMP=" + TIMESTAMP + ", CUSTOMER=" + CUSTOMER + "]";
}

public void setCVV(String cVV) {
	CVV = cVV;
}
public String getTOTAL_PRICE() {
	return TOTAL_PRICE;
}
public void setTOTAL_PRICE(String tOTAL_PRICE) {
	TOTAL_PRICE = tOTAL_PRICE;
}
public String getTIMESTAMP() {
	return TIMESTAMP;
}
public void setTIMESTAMP(String tIMESTAMP) {
	TIMESTAMP = tIMESTAMP;
}
public String getCUSTOMER() {
	return CUSTOMER;
}
public void setCUSTOMER(String cUSTOMER) {
	CUSTOMER = cUSTOMER;
}

}
