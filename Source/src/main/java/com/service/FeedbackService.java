package com.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.model.*;

@Service
public class FeedbackService {

	@Autowired
	JdbcTemplate j;

	public List<QuestionPojo> getAllQuestions() {
		List<Map<String, Object>> list = j.queryForList("SELECT * FROM questions");
		List<QuestionPojo> f = new ArrayList<QuestionPojo>();
		for (int i = 0; i < list.size() && i < 3; i++) {
			f.add(new QuestionPojo((String) list.get(i).get("QUESTION_ID"), (String) list.get(i).get("QUESTION")));

			 //System.out.println(f.get(i));
		}
		return f;
	}

	public void add(FeedbackPojo feedbackPojo){
		String sql="insert into mydb.feedbacks(user_email,q1_ans,q2_ans,q3_ans) values(?,?,?,?)";
		int x=j.update(sql,feedbackPojo.getEmail(),feedbackPojo.getAns1(),feedbackPojo.getAns2(),feedbackPojo.getAns3());
		
	}

	public List<FeedbackPojo> getAllReviews() {
		List<Map<String, Object>> list = j.queryForList("SELECT * FROM feedbacks");
		List<FeedbackPojo> f = new ArrayList<FeedbackPojo>();
		for (int i = 0; i < list.size() ; i++) {
			f.add(new FeedbackPojo((String) list.get(i).get("USER_EMAIL"), (String) list.get(i).get("Q1_ANS"),(String) list.get(i).get("Q2_ANS"),(String) list.get(i).get("Q3_ANS")));

			// System.out.println(f.get(i));
		}
		return f;
	}
	public String deleteReview(String email){
		String DELETE_QUERY = "delete from feedbacks WHERE user_Email=?;";
		int res = j.update(DELETE_QUERY, email);
//		System.out.println(res);
		return res == 1 ? "success" : "error";
		//return "heelo";
	}
	public String addQuestion(QuestionPojo questionPojo){
		List<Map<String,Object>> resu = j.queryForList("select * from questions WHERE question_id = \""+ questionPojo.getQuestion_id()+"\"");
		if(resu.size() != 0){
			String UPDATE_QUERY = "UPDATE `mydb`.`questions` SET`question`= ?   WHERE `question_id`= ? ;";
			j.update(UPDATE_QUERY, questionPojo.getQuestion(),questionPojo.getQuestion_id());
			return " udated successfully";
		}
		String sql="insert into mydb.questions(question) values(?)";
		int x=j.update(sql,questionPojo.getQuestion());
		return "added successfully";
		
	}

}
