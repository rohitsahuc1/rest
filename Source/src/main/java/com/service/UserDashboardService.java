package com.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.model.ProductPojo;

@Service
public class UserDashboardService {
	
	@Autowired
	JdbcTemplate j;

	public List<ProductPojo> getAllProducts() {
		List<Map<String, Object>> list = j.queryForList("SELECT NAME,GENRE,PRICE FROM products");
		List<ProductPojo> f = new ArrayList<ProductPojo>();
		for (int i = 0; i < list.size(); i++) {
			f.add(new ProductPojo((Integer) list.get(i).get("ID"),(Integer) list.get(i).get("STOCK"),(String) list.get(i).get("NAME"), (String) list.get(i).get("GENRE"),
					(String) list.get(i).get("PRICE")));
		}
		return f;
	}

}
