package com.service;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import com.model.PlaceOrderPojo;
import com.model.ProductPojo;
import com.model.Order;

@Service
public class OrderService {
	@Autowired
	JdbcTemplate j;
	@Autowired
	ProductsSerivce ps;

	public String addOrder(PlaceOrderPojo p) {
		String INSERT_STRING = "INSERT INTO `mydb`.`orders` (`CUSTOMER`, `TOTAL_PRICE`, `TIMESTAMP`, `ADDRESS`, `CARD_NO`, `CVV`) VALUES (?, ?, ?, ?, ?, ?);";
		String INSERT_ORDER_ITEMS = "INSERT INTO `mydb`.`order_items` (`ORDER_ID`, `PRODUCT_ID`) VALUES (?, ?)";
		KeyHolder keyHolder = new GeneratedKeyHolder();

		int response = j.update(connection -> {
			PreparedStatement ps = connection.prepareStatement(INSERT_STRING, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, p.getCUSTOMER());
			ps.setString(2, p.getTOTAL_PRICE());
			ps.setString(3, p.getTIMESTAMP());
			ps.setString(4, p.getADDRESS());
			ps.setString(5, p.getCARD_NUMBER());
			ps.setString(6, p.getCVV());
			return ps;
		}, keyHolder);

		if (response != 1)
			return "error";
		String key = keyHolder.getKey().toString();
		for (int i = 0; i < p.getItems().size(); i++) {
			response = j.update(INSERT_ORDER_ITEMS, key + "", p.getItems().get(i).getID());
			if (response != 1)
				return "error";
		}
		return "success";
	}

	public List<Order> fetchAllOrders() {
		List<Map<String, Object>> list = j.queryForList("select * from orders;");
		String oid;
		List<Map<String, Object>> olist;
		List<Order> orders = new ArrayList<Order>();
		for (int i = 0; i < list.size(); i++) {

			Order order = new Order();

			order.setID(list.get(i).get("ORDER_NO") + "");
			order.setADDRESS(list.get(i).get("ADDRESS") + "");
			order.setCARD_NO(list.get(i).get("CARD_NO") + "");
			order.setCUSTOMER(list.get(i).get("CUSTOMER") + "");
			order.setTOTAL_PRICE(list.get(i).get("TOTAL_PRICE") + "");
			order.setTIMESTAMP(Timestamp.valueOf(list.get(i).get("TIMESTAMP") + ""));
			order.setCVV(list.get(i).get("CVV") + "");

			ArrayList<ProductPojo> list2 = new ArrayList<ProductPojo>();
			oid = list.get(i).get("ORDER_NO").toString();
			// System.out.println(i);
			olist = j.queryForList("select * from order_items where ORDER_ID = " + oid);
			for (int j = 0; j < olist.size(); j++) {
				list2.add(ps.getProductById(olist.get(j).get("PRODUCT_ID") + ""));
			}
			order.setITEMS(list2);
			orders.add(order);
		}

		return orders;

	}

	public List<Order> fetchPlacedOrders(String email) {
		List<Map<String, Object>> list = j.queryForList("select * from orders ");
		String oid;
		//System.out.println(list);

		List<Map<String, Object>> olist;
		List<Order> orders = new ArrayList<Order>();
		for (int i = 0; i < list.size(); i++) {
			 System.out.println(list.get(i).get("CUSTOMER"));

			if(!list.get(i).get("CUSTOMER").equals(email)){
				continue;
			}
			Order order = new Order();

			order.setID(list.get(i).get("ORDER_NO") + "");
			order.setADDRESS(list.get(i).get("ADDRESS") + "");
			order.setCARD_NO(list.get(i).get("CARD_NO") + "");
			order.setCUSTOMER(list.get(i).get("CUSTOMER") + "");
			order.setTOTAL_PRICE(list.get(i).get("TOTAL_PRICE") + "");
			order.setTIMESTAMP(Timestamp.valueOf(list.get(i).get("TIMESTAMP") + ""));
			order.setCVV(list.get(i).get("CVV") + "");

			ArrayList<ProductPojo> list2 = new ArrayList<ProductPojo>();
			oid = list.get(i).get("ORDER_NO").toString();
			// System.out.println(i);
			olist = j.queryForList("select * from order_items where ORDER_ID = " + oid);
			for (int j = 0; j < olist.size(); j++) {
				list2.add(ps.getProductById(olist.get(j).get("PRODUCT_ID") + ""));
			}
			order.setITEMS(list2);
			orders.add(order);
		}

		return orders;

	}
	public List<Order> fetchAllOrdersByDate(Date from,Date to) {
		System.out.println("select * from orders where TIMESTAMP between "+from+" and "+to+";");
		List<Map<String, Object>> list = j.queryForList("select * from orders where TIMESTAMP between '"+from+" 00-00-00' and '"+to+" 00-00-00';");
		String oid;
		List<Map<String, Object>> olist;
		List<Order> orders = new ArrayList<Order>();
		for (int i = 0; i < list.size(); i++) {

			Order order = new Order();

			order.setID(list.get(i).get("ORDER_NO") + "");
			order.setADDRESS(list.get(i).get("ADDRESS") + "");
			order.setCARD_NO(list.get(i).get("CARD_NO") + "");
			order.setCUSTOMER(list.get(i).get("CUSTOMER") + "");
			order.setTOTAL_PRICE(list.get(i).get("TOTAL_PRICE") + "");
			order.setTIMESTAMP(Timestamp.valueOf(list.get(i).get("TIMESTAMP") + ""));
			order.setCVV(list.get(i).get("CVV") + "");

			ArrayList<ProductPojo> list2 = new ArrayList<ProductPojo>();
			oid = list.get(i).get("ORDER_NO").toString();
			// System.out.println(i);
			olist = j.queryForList("select * from order_items where ORDER_ID = " + oid);
			for (int j = 0; j < olist.size(); j++) {
				list2.add(ps.getProductById(olist.get(j).get("PRODUCT_ID") + ""));
			}
			order.setITEMS(list2);
			orders.add(order);
		}

		return orders;

	}
}
