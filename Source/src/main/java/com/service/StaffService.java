package com.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.model.ProductPojo;
import com.model.StaffPojo;

@Service
public class StaffService {
	
	
	@Autowired
	JdbcTemplate j;
	
	public List<StaffPojo> getAllStaffDetails() {
		List<Map<String, Object>> l = j.queryForList("SELECT * FROM staff");
		List<StaffPojo> s = new ArrayList<StaffPojo>();
		for (int i = 0; i < l.size(); i++) {
			s.add(new StaffPojo ((String) l.get(i).get("email"), (String) l.get(i).get("name"),(String) l.get(i).get("contact")));
		}
		return s;
	}

	public String upsertStaff(StaffPojo p) {
		System.out.println(p);
		String INSERT_QUERY = "INSERT INTO `mydb`.`staff` (`EMAIL`, `NAME`, `CONTACT`) VALUES (?, ?, ?);";
		String UPDATE_QUERY = "UPDATE `mydb`.`staff` SET`NAME`= ? , `CONTACT`= ?  WHERE `EMAIL`= ? ;";
		List<Map<String,Object>> resu = j.queryForList("select * from staff WHERE EMAIL = \""+ p.getEmail()+"\"");
		if(resu.size() != 0){
			return " Email present";
		}
		int res = j.update(INSERT_QUERY, p.getEmail(),  p.getName(),p.getContact());
		return res == 1 ? "success" : "error";

	}
	public String deleteProduct(String p) {
//		System.out.println(p);
		String DELETE_QUERY = "delete from staff WHERE Email=?;";
		int res = j.update(DELETE_QUERY, p);
//		System.out.println(res);
		return res == 1 ? "success" : "error";

	}
	
	

}
