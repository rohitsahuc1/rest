package com.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.model.*;
import com.service.FeedbackService;

@Controller
@SessionAttributes("loggedInUser")
public class FeedbackController {

	@Autowired
	FeedbackService feedbackService;

	@ModelAttribute("loggedInUser")
	public LoggedInUser loggedInUser() {
		return new LoggedInUser();
	}

	@GetMapping("/feedback")
	public String feedback(@ModelAttribute("feedback") FeedbackPojo feedbackPojo, BindingResult result,ModelMap model) {
		List<QuestionPojo> list=feedbackService.getAllQuestions();
		model.put("questions", list);
		return "user/submitfeedback";
	}

	@PostMapping("/feedback")
	public String submitFeedback(@ModelAttribute("loggedInUser") LoggedInUser loggedInUser,
			@ModelAttribute("feedback") FeedbackPojo feedbackPojo, BindingResult result, ModelMap model) {
		List<QuestionPojo> list=feedbackService.getAllQuestions();
		model.put("questions", list);
		feedbackPojo.setEmail(loggedInUser.getEmail());
		// System.out.println(loggedInUser.getEmail());
		feedbackService.add(feedbackPojo);
		model.put("message", "thank you for your  feedback");
		return "user/submitfeedback";

	}




	@ModelAttribute("ratings")
	public Map<String, String> buildRating() {
		Map<String, String> map = new HashMap<String, String>();

		map.put("5", "5");
		map.put("4", "4");
		map.put("3", "3");
		map.put("2", "2");
		map.put("1", "1");

		return map;

	}

}
