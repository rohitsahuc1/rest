package com.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import java.sql.*;

import com.model.CartPojo;
import com.model.FilterPojo;
import com.model.LoggedInUser;
import com.model.PlaceOrderPojo;
import com.model.ProductPojo;
import com.service.OrderService;
import com.service.ProductsSerivce;
import com.service.UserDashboardService;

@Controller
public class AdminController {

	@Autowired
	ProductsSerivce ps;
	@Autowired
	OrderService os;

	@GetMapping("admin/allorders")
	public String showAllOrders(@ModelAttribute("filterPojo") FilterPojo fp, BindingResult res, ModelMap map) {
		map.addAttribute("orders", os.fetchAllOrders());
		return "admin/adminorders";
	}

	@PostMapping("admin/allordersByDate")
	public String showAllOrdersByDate(@ModelAttribute("filterPojo") FilterPojo fp, BindingResult res, ModelMap map) {
		map.addAttribute("orders", os.fetchAllOrdersByDate(fp.getFrom(), fp.getEnd()));
		return "admin/adminorders";
	}
}
