package com.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.model.LoginPojo;
import com.model.ProductPojo;
import com.model.StaffPojo;
import com.service.ProductsSerivce;
import com.service.StaffService;

@Controller
public class StaffController {

	@Autowired
	StaffService staffService;

	@GetMapping("/staff")
	public String showStaff(ModelMap m) {
		List<StaffPojo> l = staffService.getAllStaffDetails();
		m.addAttribute("staff", l);
		return "admin/staff";
	}
	@GetMapping("/staff/add")
	public String showAddStaff(@ModelAttribute("staff") StaffPojo staff , BindingResult res, ModelMap model) {
		return "admin/addstaff";
	}

	
	@PostMapping("/staff/add")
	public String upsertStaff(@ModelAttribute("satff") @Valid StaffPojo newStaff, BindingResult result, ModelMap m) {
		String r = staffService.upsertStaff(newStaff);
		
		
		if (r.equals("success")) {
			String message;
			if (newStaff.getEmail() == null) {
				message = "Detail Successfully added";
			} else {
				message = "Detail Successfully Updated";
			}
			m.addAttribute("message", message);
			m.addAttribute("next", "/staff");
			m.addAttribute("nextname", "Go back to all staff");
			return "message";
		}
		m.addAttribute("message", r);
		return "message";

	}
	
	@GetMapping("/staff/delete")
	public String deleteStaff(ModelMap map,@RequestParam  String email)
	{
		System.out.println(email);
		String res=staffService.deleteProduct(email);
		List<StaffPojo> l = staffService.getAllStaffDetails();
		map.addAttribute("staff", l);
		//map.put("message", res);
		return "admin/staff";
	}
	
	
}
	

