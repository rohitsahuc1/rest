package com.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import java.sql.*;

import com.model.CartPojo;
import com.model.LoggedInUser;
import com.model.Order;
import com.model.PlaceOrderPojo;
import com.model.ProductPojo;
import com.service.OrderService;
import com.service.ProductsSerivce;
import com.service.UserDashboardService;

@Controller

@SessionAttributes({ "cartPojo", "loggedInUser" })

public class UserDashboardController {

	@Autowired
	ProductsSerivce ps;
	@Autowired
	OrderService os;

	@ModelAttribute("cartPojo")
	public CartPojo cartPojo() {
		return new CartPojo(new ArrayList<ProductPojo>(), 0);
	}

	@ModelAttribute("loggedInUser")
	public LoggedInUser loggedInUser() {
		return new LoggedInUser();
	}
 
	@GetMapping("/user/userdashboard")
	public String showProducts(ModelMap m) {
		List<ProductPojo> list = ps.getAllProducts();
		m.addAttribute("menuList", list);
		return "user/userdashboard";
	}

	@GetMapping("/cart/add")
	public String addToCart(@ModelAttribute("cartPojo") CartPojo cartPojo, @RequestParam int id, ModelMap map) {
		cartPojo.addToCart(ps.getProductById(id + ""));
		map.addAttribute("message", "Product added to cart");
		map.addAttribute("next", "/cart");
		map.addAttribute("nextname", "View");
		return "message";
	}

	@GetMapping("/cart")
	public String showCart() {
		return "user/cart";
	}

	@GetMapping("/cart/remove")
	public String removeFromCart(@ModelAttribute("cartPojo") CartPojo cartPojo, @RequestParam int id, ModelMap map) {
		cartPojo.removeFromCart(id);
		map.addAttribute("message", "Product removed from cart");
		map.addAttribute("next", "/cart");
		map.addAttribute("nextname", "View");
		return "message";
	}

	@GetMapping("/user/pay")
	public String showPaymentPage(@ModelAttribute("placeOrderPojo")  @Valid PlaceOrderPojo p, BindingResult res) {
		return "showpaymentpage";
	}

	@PostMapping("/user/pay")
	public String submitPayment(@ModelAttribute("cartPojo") CartPojo cartPojo,
			@ModelAttribute("loggedInUser") LoggedInUser loggedInUser,
			@ModelAttribute("placeOrderPojo") @Valid PlaceOrderPojo p, BindingResult res, ModelMap map) {

		
		String user = loggedInUser.getEmail();
		System.out.println(loggedInUser.getEmail()+"asdf");
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String total = "" + cartPojo.getTotal();
		p.setTIMESTAMP(timestamp.toString());
		p.setCUSTOMER(user);
		p.setTOTAL_PRICE(total);
		p.setItems(cartPojo.getItems());
		String result = os.addOrder(p);
		if (result != "success") {
			map.addAttribute("error", result);
			return "showpaymentpage";
		}
		cartPojo.setItems(new ArrayList<ProductPojo>());
		cartPojo.setTotal(0);
		map.addAttribute("item",p);
		return "message1";
	}

}
