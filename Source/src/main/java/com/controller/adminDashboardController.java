package com.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.model.FeedbackPojo;
import com.model.QuestionPojo;
import com.model.StaffPojo;
import com.service.FeedbackService;
@Controller
public class adminDashboardController {
	

	@Autowired
	FeedbackService feedbackService;
	
	

	@GetMapping("/reviews")
	public String showreviews(ModelMap model)
	{
		List<QuestionPojo> questions=feedbackService.getAllQuestions();
		List<FeedbackPojo> reviews=feedbackService.getAllReviews();
		model.put("questions", questions);
		model.put("reviews", reviews);
		//System.out.println(questions);
		return "admin/reviews";
	}

	@GetMapping("/editquestionaire")
	public String showeditquestionaire(@ModelAttribute("question") QuestionPojo questionPojo,BindingResult b,ModelMap model)
	{
		Map<String, String> map = new HashMap<String, String>();
		
		map.put("1", "1");
		map.put("2", "2");
		map.put("3", "3");
		model.put("back","/admin/admin-dashboard");

		model.put("id",map);
		return "admin/editquestionaire";
	}
	@PostMapping("/editquestionaire")
	public String showeditquestion(@ModelAttribute("question") QuestionPojo questionPojo,BindingResult result,ModelMap model)
	{
		//System.out.println("nhi");
		//System.out.println(questionPojo.getQuestion());
		String message=feedbackService.addQuestion(questionPojo);
		model.put("message", message);
		model.put("back","/admin/admin-dashboard");
		model.put("more","redirect:/editquestionaire");
		return "admin/editquestionaire";
	}
	@GetMapping("/review/delete")
	public String deleteReview(ModelMap map,@RequestParam  String email)
	{
		//System.out.println(email);
		String res=feedbackService.deleteReview(email);
		
		return "redirect:/reviews";
	}
	

}
