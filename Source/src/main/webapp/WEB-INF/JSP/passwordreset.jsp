
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Password Reset Form</title>
<link rel="stylesheet" type="text/css" href="../css/form.css"> 
</head>
<body>
	<center>
		<jsp:include page="./header.jsp" />
		<h3>Password Reset</h3>

		<sf:form modelAttribute="reset">
			<table>

				<tr>
					<td>Email</td>
					<td><sf:input path="email"  type="email" required="true" /></td>
					<td><sf:errors path="email" /></td>
				</tr>
				<tr>
					<td>Secret Question</td>
					<td><sf:select path="question" items="${list}"  required="true"  /></td>
					<td><sf:errors path="question" /></td>
				</tr>
				<tr>
					<td>Secret Answer</td>
					<td><sf:input path="answer"   required="true" /></td>
					<td><sf:errors path="answer" /></td>
				</tr>
				<tr>
					<td>Password</td>
					<td><sf:input path="password" type="password" required="true" /></td>
					<td><sf:errors path="password" /></td>
				</tr>
				<tr>
					<td>Confirm Pass</td>
					<td><sf:input path="confirm"  required="true" type="password"/></td>
					<td><sf:errors path="confirm" /></td>
				</tr>
				<tr>
					<td><button type="submit" class="registerbtn">Register</button></td>
					<td><button type="reset" class="registerbtn">Clear</button></td>

				</tr>

			</table>



		</sf:form>


	<h2>	<p class="error">${error}</p></h2>

	</center>

</body>
</html>
