
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Payment</title>
<link rel="stylesheet" type="text/css" href="../css/form.css">
<link rel="stylesheet" type="text/css" href="../css/table.css"> 
</head>
<style>

.add{
color: white;
    background-color: #4CAF50;
    padding: 6px;
    text-decoration: none;
   margin:1rem 0;
   display:block;
   width:fit-content;
    border-radius: 4px;

    height: fit-content;
    
    }
</style>
<body style="max-width: 800px;">
	<jsp:include page="header.jsp" />

	<h3>Place Order</h3>
	<a class="add" href="/cart">go back to cart</a>
	<sf:form modelAttribute="placeOrderPojo">
		<table>
			<tr>
				<td>Address</td>
				<td><sf:textarea path="ADDRESS" rows="5" cols="30" requried="true"/></td>
				<td><sf:errors path="ADDRESS" /></td>
			</tr>
				<tr>
				<td>Card Number</td>
				<td><sf:input  path="CARD_NUMBER" required="true"/></td>
				<td><sf:errors path="CARD_NUMBER" /></td>
			</tr>
			<tr>
				<td>Card Cvv</td>
				<td><sf:input type="number" maxlen="3" minlen="3"  path="CVV" required="true"/></td>
				<td><sf:errors path="CVV" /></td>
			</tr>
		
		
			<tr>
				<td><button type="submit" class="registerbtn">Submit</button></td>
				<td><button type="reset" class="registerbtn">Clear</button></td>
			</tr>

		</table>



	</sf:form>
</body>
</html>
