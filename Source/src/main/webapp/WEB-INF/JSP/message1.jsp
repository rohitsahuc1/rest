<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Message</title>
<link rel="stylesheet" type="text/css" href="../css/table.css">
<link rel="stylesheet" type="text/css" href="../css/form.css"> 
</head>
<body>
	<jsp:include page="header.jsp" />
	<p>${message}
		<a href="${next}">${nextname} </a><br>
		
	</p>

<a href="/feedback">Rate your experince</a><br><br>
 <form>
          <table id="tblData">
              <tr>
                   <td><input type="button" value="Print this page" class="registerbtn"
                        onClick="window.print()"></td>
              </tr>
        	<tr>
			
			<th>CUSTOMER</th>
			<th>TOTAL_PRICE</th>
			<th>TIMESTAMP</th>
			<th>ADDRESS</th>
			
			<th>items</th>
			
		</tr>
		
			<tr>
				
				<td>${item.CUSTOMER}</td>
				<td>${item.TOTAL_PRICE}</td>
				<td>${item.TIMESTAMP}</td>
				<td>${item.ADDRESS}</td>
				
				<td>${item.items}</td>
				
				
				
			</tr>
		
              <tr>
                   <td><input type="button" class="registerbtn" value="Export to Excel Sheet" onclick="exportTableToExcel('tblData')"></td>
              </tr>
          </table>
     </form>

</body>
<script type="text/javascript">
function exportTableToExcel(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
}
</script>


</body>
</html>
