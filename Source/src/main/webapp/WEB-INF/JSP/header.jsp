<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
body {

	max-width: 1600px;
	margin: auto;
}

.header {
	display: flex;
	justify-content: space-between;
	align-items: center;
	height: 50px;
	border-bottom: 1px solid lightgray;
}

.header .right {
	display: flex;
	align-items: center;
}

.header .h6 {
	margin: 4px;
}

.header a {
	color: white;
	background-color: #4CAF50;
	padding: 6px;
	text-decoration: none;
	flex-grow: 0;
	display: block;
	border-radius: 4px;
	margin: 0 4px;
	height: fit-content;
}

.error {
	color: red;
}
</style>
<div class="header">
	<strong><h1>Restaurant Management</h1></strong>
	<c:if test="${not empty loggedInUser.email}">
		<div class="right">

			<h2>${loggedInUser.email}</h2>
			&nbsp; &nbsp;

			<c:if test="${ loggedInUser.role != 'admin' }">
				<a href="/user/userdashboard"  class="fa fa-home">Home</a>
			</c:if>
			<c:if test="${ loggedInUser.role == 'admin' }">
				<a href="/admin/admin-dashboard"class="fa fa-home">Home</a>
			</c:if>

			<a href="/user/logout" class="registerbtn">Logout</a>
		</div>

	</c:if>
	<c:if test="${ empty loggedInUser.email}">

		<div class="right">
			<a href="/user/login">Login</a> <a href="/user/register">Register</a>
		</div>
	</c:if>
</div>
