<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Log In</title>
<link rel="stylesheet" type="text/css" href="../css/table.css">
</head>
<body>
<jsp:include page="../header.jsp" />
<center> <h1> Review Details <h1><center>
 

<table border="1" cellpadding="2" cellspacing="2">
		<tr>
			<th>ID</th>
			<th>CUSTOMER</th>
			<th>TOTAL_PRICE</th>
			<th>TIMESTAMP</th>
			<th>ADDRESS</th>
			<th>CARD_NO</th>
			<th>CVV</th>
			<th>items</th>
			
		</tr>
		<c:forEach var="item" items="${list}">
			<tr>
				<td>${item.ID}</td>
				<td>${item.CUSTOMER}</td>
				<td>${item.TOTAL_PRICE}</td>
				<td>${item.TIMESTAMP}</td>
				<td>${item.ADDRESS}</td>
				<td>${item.CARD_NO}</td>
				<td>${item.CVV}</td>
				<td>${item.ITEMS}</td>
				
				
				
			</tr>
		</c:forEach>
	</table>

<a href="<c:url value = "user/user-dashboard" />"> Go back </a> 

</body>
</html>
