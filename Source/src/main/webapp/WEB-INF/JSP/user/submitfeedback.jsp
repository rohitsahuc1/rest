<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Log In</title>
<link rel="stylesheet" type="text/css" href="../css/form.css"> 

</head>
<body>

<jsp:include page="../header.jsp" />
<center>

	<h1>Feedback Form</h1>
	<sf:form  modelAttribute="feedback">
		<table>

			<tr>
				<td>${questions[0]}</td>
				<td><sf:select path="ans1" items="${ratings}" value="5"/></td>
				<td><sf:errors path="ans1" /></td>
			</tr>
			<tr>
				<td>${questions[1]}</td>
				<td><sf:select path="ans2" items="${ratings}" value="5"  /></td>
				<td><sf:errors path="ans2"  /></td>
			</tr>
			<tr>
				<td>${questions[2]}</td>
				<td><sf:select path="ans3" items="${ratings}" value="5"/></td>
				<td><sf:errors path="ans3" /></td>
			</tr>





			<tr>
			<td></td>
				<td><button type="submit" class="registerbtn">submit</button></td>

			</tr>
		</table>
		<h1>${message}</h1>

	</sf:form>

</center>
</body>
</html>