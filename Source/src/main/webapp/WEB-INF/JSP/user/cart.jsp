<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>All Products</title>

<link rel="stylesheet" type="text/css" href="../css/table.css"> 

</head>

<body>
	<jsp:include page="../header.jsp" />
	<h3>Cart</h3>
	<div class="flex">
		<a class="add" href="user/userdashboard">Home</a><a class="add"
			href="/user/pay">Proceed to Payment </a>
	</div>
	<table>
		<tr>
			<th>Name</th>
			<th>Price</th>
			<th>Genre</th>
			<th>actions</th>
		</tr>

		<c:forEach var="cartItem" items="${cartPojo.items}">
			<tr>
				<td>${cartItem.NAME}</td>
				<td>${cartItem.PRICE}</td>
				<td>${cartItem.GENRE}</td>
				<td><a href="/cart/remove?id=${cartItem.ID}">Delete</a></td>
			</tr>
		</c:forEach>
		<tr>

			<td><strong>Total:</strong></td>
			<td>${cartPojo.total}</td>
			<td></td>
			<td></td>
		</tr>
	</table>
</body>
</html>
