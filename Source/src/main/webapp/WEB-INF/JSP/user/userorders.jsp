<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>All Products</title>
<style>
td, th {
	padding: 4px;
	color: white !important;
}

td a, th a {
	padding: 4px;
	color: white !important;
}

tr:nth-child(odd) {
	background: #2196F3;
}

tr:nth-child(even) {
	background: #8BC34A;
}

.add {
	color: white;
	background-color: #4CAF50;
	padding: 6px;
	text-decoration: none;
	margin: 1rem 0;
	display: block;
	width: fit-content;
	border-radius: 4px;
	height: fit-content;
}
</style>
</head>

<body>
	<jsp:include page="../header.jsp" />
	<h3>All Orders</h3>
	<table>
		<tr>
			<th>ID</th>
			<th>Customer Email</th>
			<th>time</th>
			<th>Address</th>
			<th><table>
					<tr>
						<th colspan="2">Items</th>
					</tr>
					<tr>
					<tr>
						<th>Name</th>
						<th>Price</th>
					<tr>
				</table></th>
		</tr>
		<c:forEach var="order" items="${orders}">

			<tr>
				<td>${order.ID}</td>
				<td>${order.CUSTOMER}</td>
				<td>${order.TIMESTAMP}</td>
				<td>${order.ADDRESS}</td>
				<td>
					<table>
						<c:forEach var="item" items="${order.ITEMS}">
							<tr style="background-color: transparent">
								<td>${item.NAME}</td>
								<td>${item.PRICE}</td>
							<tr>
						</c:forEach>
						<tr style="background-color: transparent">
							<th>Total price</th>
							<th>${order.TOTAL_PRICE}</th>
						</tr>
					</table>
				</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>