<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>All Products</title>
<link rel="stylesheet" type="text/css" href="../css/table.css">
<link rel="stylesheet" type="text/css" href="../css/form.css"> 
</head>

<body>
	<jsp:include page="../header.jsp" />
	<h3>All Orders</h3>
	<sf:form action="/admin/allordersByDate" method="POST" modelAttribute="filterPojo">
		<table>
			<tr>
				<td>Filter By Date</td>
				<td>From <sf:input type="date" path="from" />
				</td>
				<td>End <sf:input type="date" path="end" />
				</td>
				<td><Button type="submit" class="registerbtn">GO</Button></td>
			</tr>
		</table>
	</sf:form>
	<table>
		<tr>
			<th>ID</th>
			<th>Customer Email</th>
			<th>time</th>
			<th>Address</th>
			<th><table>
					<tr>
						<th colspan="2">Items</th>
					</tr>
					<tr>
					<tr>
						<th>Name</th>
						<th>Price</th>
					<tr>
				</table></th>
		</tr>
		<c:forEach var="order" items="${orders}">

			<tr>
				<td>${order.ID}</td>
				<td>${order.CUSTOMER}</td>
				<td>${order.TIMESTAMP}</td>
				<td>${order.ADDRESS}</td>
				<td>
					<table>
						<c:forEach var="item" items="${order.ITEMS}">
							<tr style="background-color: transparent">
								<td>${item.NAME}</td>
								<td>${item.PRICE}</td>
							<tr>
						</c:forEach>
						<tr style="background-color: transparent">
							<th>Total price</th>
							<th>${order.TOTAL_PRICE}</th>
						</tr>
					</table>
				</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>