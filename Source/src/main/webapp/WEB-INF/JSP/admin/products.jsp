<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/table.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>All Products</title>




</head>

<body>
	<jsp:include page="../header.jsp" />
	<center><h2 >Inventory</h2> </center><a class="add" href="/products/add">Add Products</a>
<table border="1" cellpadding="2" cellspacing="2">

		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Price</th>
			<th>Genre</th>
			<th>Stock</th>
			<th>actions</th>
		</tr>
		<c:forEach var="product" items="${products}">
			
			<tr>
			
				<td>${product.ID}</td>
				<td>${product.NAME}</td>
				<td>${product.PRICE}</td>
				<td>${product.GENRE}</td>
				<td>${product.STOCK}</td>
				<td><a href="/products/edit?id=${product.ID}">Edit</a>|<a href="/products/delete?id=${product.ID}">Delete</a></td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>
