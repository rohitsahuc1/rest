
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Staff Details</title>

<link rel="stylesheet" type="text/css" href="../css/form.css"> 
</head>
<body style="background-color:#f7e6e6;">
<jsp:include page="../header.jsp" />
<div>
<center>	<sf:form modelAttribute="staff" >
<br>
<br>
<br>
<h2> Add Staff Details</h2>

<br>
<br>
<br>
		<table>
			<tr>
				<td>Email</td>
				<td><sf:input path="email" /></td>
				<td><sf:errors path="email" /></td>
			</tr>
			<tr>
				<td>Name</td>
				<td><sf:input path="name" /></td>
				<td><sf:errors path="Name" /></td>
			</tr>

			<tr>
				<td>Contact Number</td>
				<td><sf:input path="contact" pattern="[7-9]{1}[0-9]{9}"/></td>
				<td><sf:errors path="contact" /></td>
			</tr>

			<tr>
				<td><button type="submit" class="registerbtn">Register</button></td>
				<td><button type="reset" class="registerbtn">Clear</button></td>

			</tr>

		</table>



	</sf:form>


</center>
</div>

</body>
</html>