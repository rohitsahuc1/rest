
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registration Form</title>


<link rel="stylesheet" type="text/css" href="../css/form.css"> 
</head>
<body>
	<jsp:include page="../header.jsp" />
<center>	<h3>Add Product</h3></center>
	<sf:form modelAttribute="product" action="/products/upsert">
	<center>	<table>

			<tr>
				<td>Name of the product</td>
				<td><sf:input path="NAME" /></td>
				<td><sf:errors path="NAME" /></td>
			</tr>
			<tr>
				<td>Genre</td>
				<td><sf:input path="GENRE" /></td>
				<td><sf:errors path="GENRE" /></td>
			</tr>
			<tr>
				<td>Price</td>
				<td><sf:input path="PRICE" type="number"/></td>
				<td><sf:errors path="PRICE" /></td>
			</tr>

			<tr>
				<td>Stock</td>
				<td><sf:input path="STOCK" type="number" value="0"/></td>
				<td><sf:errors path="STOCK" /></td>
			</tr>
			
			<tr>
				<td><button type="submit" class="registerbtn">Add Product</button></td>
				<td><button type="reset" class="registerbtn"> Clear</button></td>

			</tr>

		</table>
		</center>
	</sf:form>






</body>
</html>
