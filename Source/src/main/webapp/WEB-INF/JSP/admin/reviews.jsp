<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Log In</title>
<link rel="stylesheet" type="text/css" href="../css/table.css">


</head>
<body>
<jsp:include page="../header.jsp" />
<center> <h1> Review Details <h1><center>
 

<table border="1" cellpadding="2" cellspacing="2">
		<tr>
			<th>Email</th>
			<th>${questions[0]}</th>
			<th>${questions[1]}</th>
			<th>${questions[2]}</th>
			<th>Action</th>
			
			
		</tr>
		<c:forEach var="review" items="${reviews}">
			<tr>
				<td>${review.email}</td>
				<td>${review.ans1 }</td>
				<td>${review.ans2 }</td>
				<td>${review.ans3 }</td>
				
				<td><a href="/review/delete?email=${review.email}">Delete</a></td>
			</tr>
		</c:forEach>
	</table>

<a href="<c:url value = "admin/admin-dashboard" />"> Go back </a> 

</body>
</html>
