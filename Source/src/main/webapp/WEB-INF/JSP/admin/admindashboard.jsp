<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Log In</title>




<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>



</head>
<body style= "background-color:white;">
	<jsp:include page="../header.jsp" />

<br>
<br>





		
		<br>
		<div class="row">
			<div class="col-sm-2"> 
				<p class="text-center">
				<h2>&nbsp;&nbsp;	&nbsp;&nbsp;<strong>Staff</strong></h2>
				</p>
				<br> 
				<a	href="<c:url value = "/staff"/>"> <img src="../image/1.jfif"
						class="img-circle person" alt="Random Name" width="250"
						height="250">
				</a>
				

			</div>
		
			<div class="col-sm-2">
				<p class="text-center">
				<h2>	&nbsp;&nbsp;&nbsp;<strong>Product</strong></h2>
				</p>
				<br> 
				<a	href="<c:url value = "/products"/>"> <img src="../image/2.jpg"
						class="img-circle person" alt="Random Name" width="250"
						height="250"></a>
				</a>

			</div>
			<div class="col-sm-2">
				<p class="text-center">
			<h2>		&nbsp;&nbsp;<strong>Review </strong></h2>
				</p>
				<br> 	
				 	<a href="<c:url value = "/reviews"/>"><img	src="../image/review.png" class="img-circle person" alt="Random Name"
					width="250" height="250">
				</a>

			</div>
			<div class="col-sm-2">
			
				<p class="text-center">
				<h2>	&nbsp;&nbsp;&nbsp;&nbsp;<strong>Edit Question </strong></h2>
				</p>
				<br>  
				<a href="<c:url value = "/editquestionaire"/>"> <img
						src="../image/edit.png" class="img-circle person" alt="Random Name"
						width="250" height="250">
				</a>
			</div>
			<div class="col-sm-2">
			
				<p class="text-center">
				<h2>&nbsp;&nbsp;&nbsp;	&nbsp;&nbsp;&nbsp;<strong>Orders </strong></h2>
				</p>
				<br>  
				<a href="<c:url value = "/admin/allorders"/>"> <img
						src="../image/order.jfif" class="img-circle person" alt="Random Name"
						width="250" height="250">
				</a>
			</div>
		</div>
<br>
<br>
<br>




		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<img src="../image/1.jpg" alt="New York" width="1200" height="700">
					<div class="carousel-caption">
						<h3>About us</h3>
						<p>Astronomy compels the soul to look upward, and leads us from this world to another. Curious that we spend more time congratulating people who have succeeded than encouraging people who have not. As we got further and further away, it [the Earth] diminished in size..</p>
					</div>
				</div>

				<div class="item">
					<img src="../image/2.jpg" alt="Chicago" width="1200" height="700">
					<div class="carousel-caption">
						<h3>Have A Look To Our Dishes</h3>
						<p>Thank you, Chicago - A night we won't forget.</p>
					</div>
				</div>

				<div class="item">
					<img src="../image/4.jpg" alt="Los Angeles" width="1200"
						height="700">
					<div class="carousel-caption">
						<h3>LA</h3>
						<p>Even though the traffic was a mess, we had the best time
							playing at Venice Beach!</p>
					</div>
				</div>
			</div>

			<!-- Left and right controls -->
			<a class="left carousel-control" href="#myCarousel" role="button"
				data-slide="prev"> <span
				class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a> <a class="right carousel-control" href="#myCarousel" role="button"
				data-slide="next"> <span
				class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
</body>
</html>
