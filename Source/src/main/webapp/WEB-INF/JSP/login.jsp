<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Log In</title>

<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="../css/form.css"> 
<style>
body{
background-image:url('../image/1.jpg');
 
}
</style>

</head>

<body>
<jsp:include page="HomeHeader.jsp" />
<br>
<br>
<br> 
<center><h3>Login Form</h3></center>
<br>
<br>
	<sf:form method="post" modelAttribute="login">
		<table>
			<tr>
				<td>Email Id</td>
				<td><sf:input path="emailId" required="true"/></td>
				<td><sf:errors path="emailId" /></td>
			</tr>
			<tr>
				<td>Password</td>
				<td><sf:input type="password" path="password" required="true" /></td>
				<td><sf:errors path="password" /></td>
			</tr>
			<tr>
				<td>Role</td>
				<td><sf:select path="role" items="${role}" /></td>
				<td><sf:errors path="role" /></td>
			</tr>

			<tr>
				<td><button type="submit" class="registerbtn">Login</button></td>

			</tr>
		</table>


	</sf:form>
	<a href="/passwordreset" ><button type="submit"  class="registerbtn button2">Forget
			Password</button></a>
	<p>${error}</p>
</body>
</html>